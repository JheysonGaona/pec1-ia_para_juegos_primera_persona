using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

namespace Pec1 {

    // Se obliga a requerir el componente
    [RequireComponent(typeof(NavMeshAgent))]

    public class CharacterAi : MonoBehaviour {

        // Referencia al objeto que dispone de los puntos de control
        [SerializeField] private Transform wayPoints;

        // velocidad mínima del agente
        [SerializeField] private float speedMin = 3.0f;

        // velocidad máxima del agente
        [SerializeField] private float speedMax = 6.0f;

        // Distancia a la cual debe detenerse el agente sobre su objetivo
        [SerializeField] private float stopDistance = 0.1f;


        // velocidad actual del agente
        private float currentSpeed;

        // Referencia al componente de AI
        private NavMeshAgent navMeshAgent;

        // Referencia a la lista de los puntos de control
        private readonly List<Transform> allWayPoints = new ();

        // Nueva lista que almacena los waypoints suavizados
        private readonly List<Vector3> smoothedWayPoints = new ();


        // Método de llamada de Unity, se llama una única vez al iniciar el aplicativo
        // Se procede a configurar el componente de AI
        private void Awake() {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }


        // Método de llamada de Unity, se llama una única vez al iniciar el aplicativo, después de Awake
        // Se procede a configurar el agrente y las variables iniciales
        private void Start() {
            // Se agrega a la lista todos los puntos de control
            foreach (Transform current in wayPoints){
                allWayPoints.Add(current);
            }
            // Se asigna la velocidad de movimiento aleatoria para el agrente
            currentSpeed = Random.Range(speedMin, speedMax);

            // Suavizar los waypoints
            SmoothWaypoints();

            // Ajustes del agente, y su destino inicial
            navMeshAgent.updateRotation = true;
            navMeshAgent.speed = currentSpeed;
            SetRandomDestination();
        }


        // Método de llamada de Unity, se llama en cada frame del computador
        // Se realiza la lógica de movimiento del agente
        private void Update() {
            // Se valida que el agente haya llegado a su destino
            if (navMeshAgent.remainingDistance <= stopDistance) {
                SetRandomDestination();
            }
        }


        // Método privado que permite establecer aleatoriamente una dirección
        private void SetRandomDestination() {
            int randomIndex = Random.Range(0, smoothedWayPoints.Count);
            navMeshAgent.destination = smoothedWayPoints[randomIndex];
        }


        // Función para suavizar los waypoints usando Catmull-Rom
        private void SmoothWaypoints() {
            smoothedWayPoints.Clear();
            // Se valida que al menos haya cuatro puntos de referencia
            if (allWayPoints.Count < 4) {
                // Se reccore y asigna cada punto de referencia
                foreach (Transform waypointTransform in allWayPoints) {
                    // Convertir transformación a Vector3
                    smoothedWayPoints.Add(waypointTransform.position);
                }
                return;
            }

            // Se recorre la lista de todos los puntos de control
            for (int i = 1; i < allWayPoints.Count - 2; i++) {
                for (float t = 0; t <= 1; t += 0.1f) {
                    Vector3 p0 = allWayPoints[i - 1].position; // Convertir transformación a Vector3
                    Vector3 p1 = allWayPoints[i].position;     // Convertir transformación a Vector3
                    Vector3 p2 = allWayPoints[i + 1].position; // Convertir transformación a Vector3
                    Vector3 p3 = allWayPoints[i + 2].position; // Convertir transformación a Vector3

                    Vector3 newPos = CatmullRomSpline(p0, p1, p2, p3, t);
                    smoothedWayPoints.Add(newPos);
                }
            }
        }


        // Función de interpolación Catmull-Rom Spline
        private Vector3 CatmullRomSpline(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
            float tt = t * t;
            float ttt = tt * t;
            float q1 = -ttt + 2.0f * tt - t;
            float q2 = 3.0f * ttt - 5.0f * tt + 2.0f;
            float q3 = -3.0f * ttt + 4.0f * tt + t;
            float q4 = ttt - tt;

            Vector3 newPos = 0.5f * (q1 * p0 + q2 * p1 + q3 * p2 + q4 * p3);

            return newPos;
        }


    #if UNITY_EDITOR
        // Función para dibujar Gizmos para puntos de ruta suavizados
        private void OnDrawGizmos() {
            Gizmos.color = Color.blue;

            for (int i = 1; i < smoothedWayPoints.Count; i++){
                Gizmos.DrawLine(smoothedWayPoints[i - 1], smoothedWayPoints[i]);
            }
        }
    #endif
    }
}