using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

namespace Pec1 {

    [RequireComponent(typeof(NavMeshAgent))]

    public class Elder : MonoBehaviour {

        // Referencia al objeto que dispone de los puntos de control
        [SerializeField] private Transform wayPoints;

        // Distancia a la cual debe detenerse el agente sobre su objetivo
        [SerializeField] private float stopDistance = 0.1f;

        // Velocidad de movimiento del agente
        [SerializeField] private float speed = 1.5f;

        private float initialSpeed;

        // Referencia al componente de agente AI
        private NavMeshAgent navMeshAgent;

        
        // Acceso a los estados
        [HideInInspector] public IStateElder currentState;
        [HideInInspector] public StateWander stateWander;
        [HideInInspector] public StateIdle stateIdle;



        // Método de llamada de Unity, se llama una única vez al iniciar el aplicativo
        // Se procede a configurar el componente de AI y los estados
        private void Awake() {
            stateWander = new StateWander (this, wayPoints);
            stateIdle = new StateIdle (this);
            navMeshAgent = GetComponent<NavMeshAgent>();
        }


        // Método de llamada de Unity, se llama una única vez al iniciar el aplicativo, después de Awake
        // Se indica cuál es el estado inicial
        private void Start () {
            currentState = stateWander;
            initialSpeed = speed;
            navMeshAgent.stoppingDistance = 0.1f;
            SetSpeed();
        }

        
        // Método de llamada de Unity, se llama en cada frame del computador
        // Se realiza la lógica que permite cambiar a otro estado
        private void Update () {
            currentState.UpdateState();
        }


        // Método de llamada de Unity, permite validar si el gameObject ingresa
        // dentro de un collider marcado como disparador
        private void OnTriggerEnter(Collider other) {
            // Se valida si ha ingresado a una silla
            if(other.name.Equals("Bench")){
                currentState.AStateIdle();
            }
        }


        private void SetSpeed(){
            navMeshAgent.speed = speed;
        }


        // Se realiza la lógica que permite validar si el agente a llegado al destino
        public void ValidateDestination () {
            if (navMeshAgent.remainingDistance <= stopDistance) {
                navMeshAgent.destination = currentState.AStateWander();
            }
        }


        /// Método público que permite al agente descansar
        /// <param name="time">Tiempo de descanso</param>
        public void BreakTime(int time){
            StartCoroutine(CoroutineBreakTime(time));
        }


        // Coroutina de descanso
        /// <param name="time">Tiempo de descanso</param>
        private IEnumerator CoroutineBreakTime(int time){
            navMeshAgent.speed = 0;
            yield return new WaitForSeconds(time);
            currentState.AStateWander();
            SetSpeed();
        }
    }
}