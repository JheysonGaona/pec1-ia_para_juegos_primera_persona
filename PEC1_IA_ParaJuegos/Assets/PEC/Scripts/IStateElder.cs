using UnityEngine;

namespace Pec1{

    public interface IStateElder {
        
        // Actualiza el estado
        void UpdateState();
        
        // Actualiza el método de deambular
        Vector3 AStateWander();

        // Actualiza el método de descansar
        void AStateIdle();
    }
}