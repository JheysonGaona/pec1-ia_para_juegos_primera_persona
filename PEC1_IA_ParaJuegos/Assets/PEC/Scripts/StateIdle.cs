using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pec1{

    public class StateIdle : IStateElder {

        // Referencia al script 'Elder'
        private readonly Elder elder;

        // Si es verdadero el agente esta descanzado, caso contrario no
        private bool idle = true;


        public StateIdle(Elder elder){
            this.elder = elder;
        }


        // Método que permite actualizar el estado
        public void UpdateState() {
            // Se valida el estado de descanzo
            if(idle) {
                idle = false;
                int random = Random.Range(3, 10);
                elder.BreakTime(random);
            }
        }


        public void AStateIdle() {

        }
        

        // Actualiza el método de deambular
        /// <returns>Devuelve el nuevo destino del agrente</returns>
        public Vector3 AStateWander() {
            elder.currentState = elder.stateWander;
            return Vector3.zero;
        }
    }
}