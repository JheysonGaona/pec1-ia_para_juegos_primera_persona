using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Pec1{

     public class ManagerSpawn : MonoBehaviour {

        [Header("Prefab NPC & WayPoints")]
        // Referencia al prefab del npc que se instancia dentro del escenario
        [SerializeField] private GameObject prefabNpc;

        // Referencia a los puntos de control que recorren los npc
        [SerializeField] private GameObject wayPointsNpc;

        // Radio o distancia hasta donde se pueden spamear los npc
        [SerializeField] private float areaRadius = 10f;

        // Número de npc que se generan
        [SerializeField] private int limitSpawnNpc;

        // Lista de nombres de áreas sobre el cual el personaje no puede escoger ruta dentro del navmesh
        [SerializeField] private string[] excludedAreas = new string[] {"Not Walkable"};



        // Se utiliza para saber cuantos npc se encuentran disponibles
        private int currentNpcCount = 0;

        // Capas sobre el cual el personaje no puede caminar o escoger una ruta
        private int walkableAreaMask;
        

    
        // Método privado que permite spamear un nuevo npc dentro de un área
        private void SpawnNpc(){
            while(currentNpcCount < limitSpawnNpc){
                // Se busca una nueva dirección dentro de la zona inicial de movimiento
                Vector3 randomDirection = transform.position + Random.insideUnitSphere * areaRadius;
                NavMesh.SamplePosition(randomDirection, out NavMeshHit hit, areaRadius, 1);

                // Se crea y genera el nuevo npc en un punto dentro del área permitida
                GameObject newNpc = Instantiate(prefabNpc, hit.hit ? hit.position: transform.position, Quaternion.identity);

                Transform newPositionTransform = new GameObject("NewPosition").transform;
                newPositionTransform.position = hit.position;

                newPositionTransform.parent = transform;

                // Se busca el script 'NpcStats' y 'NpcMovement' para suscribir el evento
                // newNpc.GetComponent<NpcStats>().NpcManager = this;
                // newNpc.GetComponent<NpcMovement>().SetCheckpoints(newPositionTransform, wayPointsNpc);

                currentNpcCount++;
            }
        }
    }
}