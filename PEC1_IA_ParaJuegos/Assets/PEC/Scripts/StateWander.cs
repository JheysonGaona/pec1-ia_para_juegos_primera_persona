using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Pec1{

    public class StateWander : IStateElder {

        // Referencia al script 'Elder'
        private readonly Elder elder;

        // Referencia a la lista de los puntos de control
        private readonly List<Transform> allWayPoints = new ();


        /// Constructor con parámetros
        /// <param name="elder">Referencia al script 'Elder'</param>
        /// <param name="wayPoints">Transform que contiene los puntos de control</param>
        public StateWander(Elder elder, Transform wayPoints){
            this.elder = elder;
            // Se recorren todos los hijos de wayPoints y se agrega a la lista
            foreach(Transform current in wayPoints){
                allWayPoints.Add(current);
            }
        }


        // Método privado que realiza la lógica de actualización
        public void UpdateState() {
            elder.ValidateDestination();
        }


        public void AStateIdle() {
            elder.currentState = elder.stateIdle;
        }


        // Actualiza el método de deambular
        /// <returns>Devuelve el nuevo destino del agrente</returns>
        public Vector3 AStateWander() {
            // Se asigna un punto aleatorio de la lista
            int randomIndex = Random.Range(0, allWayPoints.Count);
            return allWayPoints[randomIndex].position;
        }
    }
}